package org.liubility.robot.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * @Author JDragon
 * @Date 2021.02.18 下午 11:43
 * @Email 1061917196@qq.com
 * @Des:
 */

@Data
@Configuration
@ConfigurationProperties(prefix = "thread-pool")
public class TaskThreadPoolProperty {
    private int corePoolSize;

    private int maxPoolSize;

    private int queueCapacity;

    private String namePrefix;
}
