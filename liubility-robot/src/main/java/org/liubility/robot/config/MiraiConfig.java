package org.liubility.robot.config;

import lombok.extern.slf4j.Slf4j;
import net.mamoe.mirai.Bot;
import net.mamoe.mirai.BotFactory;
import net.mamoe.mirai.utils.BotConfiguration;
import org.liubility.robot.handler.PostOrderHandler;
import org.liubility.robot.handler.PostOrderManager;
import org.liubility.robot.handler.TestHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * @Author: Jdragon
 * @email: 1061917196@qq.com
 * @Date: 2020.10.20 11:30
 * @Description:
 */
@Slf4j
@Configuration
public class MiraiConfig {


    @Autowired
    private TestHandler testHandler;

    @Autowired
    private PostOrderManager postOrderManager;

    @Autowired
    private PostOrderHandler postOrderHandler;

    @Bean
    public Bot bot() {
        // 使用自定义配置
        BotConfiguration botConfiguration = new BotConfiguration();
        botConfiguration.fileBasedDeviceInfo("deviceInfo.json");
        botConfiguration.setProtocol(BotConfiguration.MiraiProtocol.ANDROID_PAD);
        Bot bot = BotFactory.INSTANCE.newBot(298880551, "aa456+++", botConfiguration);
//        bot.getEventChannel().registerListenerHost(testHandler);
        bot.getEventChannel().registerListenerHost(postOrderManager);
        bot.getEventChannel().registerListenerHost(postOrderHandler);
        return bot;
    }
}
