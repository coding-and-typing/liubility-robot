package org.liubility.robot.constant;


import lombok.Getter;
import net.mamoe.mirai.event.events.FriendMessageEvent;
import net.mamoe.mirai.event.events.GroupMessageEvent;
import net.mamoe.mirai.event.events.MessageEvent;

/**
 * @Author: Jdragon
 * @email: 1061917196@qq.com
 * @Date: 2020.09.04 13:37
 * @Description:
 */
public enum MessageType {

    ALL(0, "全部信息"),
    GROUP(1, "群哪信息"),
    FRIEND(2, "私聊信息");

    @Getter
    private String des;

    @Getter
    private Integer mode;

    MessageType(Integer mode, String des) {
        this.des = des;
        this.mode = mode;
    }

    public static Integer getModeByEvent(MessageEvent event) {
        if (event instanceof GroupMessageEvent) {
            return MessageType.GROUP.getMode();
        } else if (event instanceof FriendMessageEvent) {
            return MessageType.FRIEND.getMode();
        } else {
            return MessageType.ALL.getMode();
        }
    }
}
