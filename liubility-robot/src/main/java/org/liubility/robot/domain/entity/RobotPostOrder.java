package org.liubility.robot.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @Author JDragon
 * @Date 2021.02.17 下午 3:10
 * @Email 1061917196@qq.com
 * @Des:
 */

@EqualsAndHashCode(callSuper = true)
@Data
@TableName("robot_post_order")
public class RobotPostOrder extends Model<RobotPostOrder> {

    @TableId(type = IdType.AUTO)
    private Integer id;

    private String orderStr;

    private String url;

    private String param;

    //all or group or friend
    //0 or 1 or 2
    private int mode;

    //get or post
    //1 or 2
    private int request;

    private String remark;

    @Override
    protected Serializable pkVal() {
        return id;
    }
}
