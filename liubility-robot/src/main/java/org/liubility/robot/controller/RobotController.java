package org.liubility.robot.controller;

import net.mamoe.mirai.Bot;
import org.liubility.commons.http.response.normal.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.Executor;

/**
 * @Author JDragon
 * @Date 2021.02.18 下午 11:08
 * @Email 1061917196@qq.com
 * @Des:
 */
@RestController
@RequestMapping
public class RobotController {

    @Autowired
    private Executor asyncServiceExecutor;

    @Autowired
    private Bot bot;

    @GetMapping("open")
    public Result<String> open(){
        asyncServiceExecutor.execute(()->{
            bot.login();
            bot.join();
        });
        return Result.success("启动成功");
    }
}
