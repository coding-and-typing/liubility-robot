package org.liubility.robot.service.impl;

import org.liubility.commons.dto.robot.RobotMsgResult;
import org.liubility.robot.domain.entity.RobotPostOrder;
import org.liubility.robot.service.PostOrderHandlerService;
import org.liubility.robot.untils.MessageBuild;
import org.liubility.robot.untils.PostOrderListCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author: Jdragon
 * @email: 1061917196@qq.com
 * @Date: 2020.10.25 16:26
 * @Description:
 */
@Service
public class PostOrderHandlerServiceImpl implements PostOrderHandlerService {

    @Autowired
    private PostOrderListCache postOrderListCache;

    @Override
    public boolean refresh() {
        return postOrderListCache.refresh();
    }

    @Override
    public String handlePostOrder(String message, int model, Long senderId) {
        String[] s = message.split(" ");
        for (RobotPostOrder robotPostOrder : postOrderListCache.get()) {
            int mode = robotPostOrder.getMode();
            if (mode != model && mode != 0) continue;
            if (!s[0].equals(robotPostOrder.getOrderStr())) continue;
            RobotMsgResult result = PostOrderHandlerApi.robotHandle(robotPostOrder.getUrl(), robotPostOrder.getRequest(), robotPostOrder
                    .getParam(), MessageBuild.unpackMessage(senderId, s[1]));
            return result.getMessage();
        }
        return null;
    }
}
