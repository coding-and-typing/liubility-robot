package org.liubility.robot.service;


import com.baomidou.mybatisplus.extension.service.IService;
import org.liubility.robot.domain.entity.RobotPostOrder;
import org.liubility.robot.mappers.PostOrderMapper;

/**
 * @Author: Jdragon
 * @email: 1061917196@qq.com
 * @Date: 2020.10.24 22:05
 * @Description:
 */
public interface PostOrderManagerService extends IService<RobotPostOrder> {

    /**
     * @date: 2020.10.25 下午 4:21
     * @Description: 获取指令注册列表，并拼接成字符串返回
    **/
    public String getOrderListStr();

    /**
     * @date: 2020.10.25 下午 4:22
     * @Description: 获取指令注册模板
    **/
    public String getRegistrationTemplate();

    /**
     * @date: 2020.10.25 下午 4:23
     * @Description: 注册指令
    **/
    public String register(Long register, RobotPostOrder robotPostOrder);


    /**
     * @date: 2020.10.25 下午 4:23
     * @Description: 获取指令管理帮助
    **/
    public String getHelp();
}
