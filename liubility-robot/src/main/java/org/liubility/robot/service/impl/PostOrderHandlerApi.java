package org.liubility.robot.service.impl;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import org.liubility.commons.dto.robot.RobotMsgResult;
import org.liubility.commons.http.HttpUtils;
import org.liubility.commons.http.response.normal.Result;
import org.liubility.commons.json.JsonUtils;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @Author JDragon
 * @Date 2021.02.17 下午 3:14
 * @Email 1061917196@qq.com
 * @Des:
 */
@Service
public class PostOrderHandlerApi {
    public static RobotMsgResult robotHandle(String url, int request, String param, RobotMsgResult robotMsgResult) {
        try{
            HttpUtils httpUtils = HttpUtils.initJson();
            Map<String, String> map;

            if (robotMsgResult == null) return new RobotMsgResult();

            if (StrUtil.isNotBlank(param))
                robotMsgResult.setParam(param);
            String s = JsonUtils.object2Str(robotMsgResult);
            map = JSON.parseObject(s, new TypeReference<Map<String, String>>() {
            });
            if (request == 1) {
                httpUtils.setParamMap(map);
                map = httpUtils.get(url);
            } else {
                httpUtils.setBody(map);
                map = httpUtils.post(url);
            }
            Result<RobotMsgResult> result = JSON.parseObject(HttpUtils.checkResult(map), new TypeReference<Result<RobotMsgResult>>() {
            });
            return result.getResult();
        }catch (Exception e){
            return new RobotMsgResult();
        }
    }
}
