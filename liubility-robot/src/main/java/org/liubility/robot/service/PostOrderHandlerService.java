package org.liubility.robot.service;

/**
 * @Author: Jdragon
 * @email: 1061917196@qq.com
 * @Date: 2020.10.25 16:25
 * @Description:
 */
public interface PostOrderHandlerService {

    public boolean refresh();

    public String handlePostOrder(String message,int model,Long senderId);
}
