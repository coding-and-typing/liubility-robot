package org.liubility.robot.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Author: Jdragon
 * @email: 1061917196@qq.com
 * @Date: 2020.10.25 00:08
 * @Description:
 */

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface OrderMapping {

    /**
     * @Description: 除掉 split 的指令 例 #查询 设置value=“查询”即可
    **/
    String value() default "";


    /**
     * @Description: 指令前缀和参数分隔符
     **/
    String split() default "#";

    /**
     * @Description: 是否需要程序帮助回复
    **/
    boolean reply() default true;
}
