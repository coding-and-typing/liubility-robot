package org.liubility.robot;


import org.liubility.commons.zFeign.ZFeignScan;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @Author: Jdragon
 * @email: 1061917196@qq.com
 * @Date: 2020.10.20 11:32
 * @Description:
 */

@SpringBootApplication
@ZFeignScan("org.liubility.api.zFeign")
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class);
    }
}