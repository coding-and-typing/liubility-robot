package org.liubility.robot.handler;

import lombok.extern.slf4j.Slf4j;
import net.mamoe.mirai.event.EventHandler;


import net.mamoe.mirai.event.events.MessageEvent;
import org.liubility.robot.handler.base.BaseHandler;
import org.liubility.robot.annotation.OrderMapping;
import org.liubility.robot.constant.MessageType;
import org.liubility.robot.service.PostOrderHandlerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;


/**
 * @Author: Jdragon
 * @email: 1061917196@qq.com
 * @Date: 2020.09.04 12:47
 * @Description:
 */
@Slf4j
@Controller
public class PostOrderHandler extends BaseHandler {

    @Autowired
    private PostOrderHandlerService postOrderHandlerService;

    @EventHandler
    public void handler(MessageEvent event) {
        super.invoke(event);
    }

    @OrderMapping(reply = false)
    public void handlePostOrder(MessageEvent event) {
        String result = postOrderHandlerService.handlePostOrder(
                event.getMessage().contentToString(),
                MessageType.getModeByEvent(event),
                event.getSender().getId());
        if (result != null)
            event.getSubject().sendMessage(result);
    }

    @OrderMapping("指令刷新")
    public String refresh(MessageEvent event) {
        return postOrderHandlerService.refresh() ? "刷新成功" : "刷新失败";
    }
}
