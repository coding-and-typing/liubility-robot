package org.liubility.robot.handler;

import lombok.extern.slf4j.Slf4j;
import net.mamoe.mirai.contact.Group;
import net.mamoe.mirai.event.EventHandler;
import net.mamoe.mirai.event.events.GroupMessageEvent;
import net.mamoe.mirai.event.events.MessageEvent;
import net.mamoe.mirai.message.data.Image;
import net.mamoe.mirai.message.data.MessageChain;
import net.mamoe.mirai.message.data.MessageChainBuilder;
import net.mamoe.mirai.message.data.SingleMessage;
import net.mamoe.mirai.utils.ExternalResource;
import org.liubility.robot.annotation.OrderMapping;
import org.liubility.robot.handler.base.BaseHandler;
import org.springframework.stereotype.Controller;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author JDragon
 * @Date 2021.02.18 下午 3:31
 * @Email 1061917196@qq.com
 * @Des:
 */
@Slf4j
@Controller
public class TestHandler extends BaseHandler {
    @EventHandler
    public void handle(MessageEvent event) {
        super.invoke(event);
    }

    @OrderMapping("测试")
    public String help(MessageEvent event) {
        return "你好，测试";
    }

    @EventHandler
    public void onGroupMessage(GroupMessageEvent event) {
        MessageChain chain = event.getMessage();
        Group subject = event.getSubject();
        List<SingleMessage> collect = chain.stream().filter(Image.class::isInstance).collect(Collectors.toList());
        MessageChainBuilder builder = new MessageChainBuilder();
        for (SingleMessage singleMessage : collect) {
            Image image = (Image) singleMessage;
            String imageId = image.getImageId();
            builder.append(Image.fromId(imageId));
            String path = ClassLoader.getSystemResource("").getPath();
            Image image1 = subject.uploadImage(ExternalResource.create(new File(path, "1.png")));
            builder.append(image1);
        }
        if (builder.size() > 0) {
            MessageChain messageChain = builder.build();
            subject.sendMessage(messageChain);
        }
    }
}
