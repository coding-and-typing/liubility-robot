package org.liubility.robot.handler;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import net.mamoe.mirai.event.EventHandler;

import net.mamoe.mirai.event.events.MessageEvent;
import org.liubility.robot.domain.entity.RobotPostOrder;
import org.liubility.robot.handler.base.BaseHandler;
import org.liubility.robot.annotation.OrderMapping;
import org.liubility.robot.service.PostOrderManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * @Author: Jdragon
 * @email: 1061917196@qq.com
 * @Date: 2020.10.19 23:44
 * @Description:
 */
@Slf4j
@Controller
public class PostOrderManager extends BaseHandler {

    @Autowired
    private PostOrderManagerService postOrderManagerService;

    @EventHandler
    public void handle(MessageEvent event) {
        super.invoke(event);
    }

    @OrderMapping("注册帮助")
    public String help(MessageEvent event) {
        return postOrderManagerService.getHelp();
    }

    @OrderMapping("指令注册列表")
    public String orderList(MessageEvent event) {
        return postOrderManagerService.getOrderListStr();
    }

    @OrderMapping("指令注册模板")
    public String registrationTemplate(MessageEvent event) {
        return postOrderManagerService.getRegistrationTemplate();
    }

    @OrderMapping("指令注册")
    public String register(MessageEvent event) {
        String message = event.getMessage().contentToString();

        String orderStr = message.replace("#指令注册 ", "");
        RobotPostOrder robotPostOrder = JSON.parseObject(orderStr, RobotPostOrder.class);
        return postOrderManagerService.register(event.getSender().getId(), robotPostOrder);
    }
}