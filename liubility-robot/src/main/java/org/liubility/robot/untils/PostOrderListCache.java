package org.liubility.robot.untils;

import lombok.extern.slf4j.Slf4j;
import org.liubility.robot.domain.entity.RobotPostOrder;
import org.liubility.robot.service.PostOrderManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: Jdragon
 * @email: 1061917196@qq.com
 * @Date: 2020.10.20 12:48
 * @Description:
 */
@Slf4j
@Component
public class PostOrderListCache {

    @Autowired
    private PostOrderManagerService postOrderManagerService;

    private List<RobotPostOrder> postOrderList = new ArrayList<>();

    @PostConstruct
    public boolean refresh() {
        try {
            postOrderList = postOrderManagerService.list();
            return true;
        } catch (Exception e) {
            log.warn("获取注册列表失败");
            return false;
        }
    }



    public List<RobotPostOrder> get() {
        return postOrderList;
    }
}
