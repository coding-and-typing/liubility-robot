package org.liubility.robot.untils;


import org.liubility.commons.dto.robot.RobotMsgResult;

/**
 * @Author: Jdragon
 * @email: 1061917196@qq.com
 * @Date: 2020.10.20 12:38
 * @Description:
 */
public class MessageBuild {
    public static RobotMsgResult unpackMessage(Long senderId, String message) {
        return RobotMsgResult.builder()
                .message(message)
                .senderId(senderId).build();
    }
}
