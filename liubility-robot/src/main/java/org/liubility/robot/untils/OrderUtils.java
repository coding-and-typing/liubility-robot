package org.liubility.robot.untils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @Author: Jdragon
 * @email: 1061917196@qq.com
 * @Date: 2020.10.25 16:30
 * @Description:
 */
public class OrderUtils {


    //将 #{指令} 中的 {指令} 提取出来
    public static String findOrder(String message){
        String pattern = "(?<=^#)(\\S*[^\\s]|.+)";
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(message);

        if (m.find()) {
            return m.group();
        }
        return null;
    }
}
